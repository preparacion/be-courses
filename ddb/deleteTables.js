/**
 * ddb/deleteTables.js
 *
 * @description :: Deletes tables
 * @docs        :: TODO
 */

const { deleteLocationsTable } = require('./locations')
const { deleteClassRoomsTable } = require('./classRooms')
const { deleteUsersTable } = require('./users')
const { deleteSchedulesTable } = require('./schedules')
const { deleteCoursesTable } = require('./courses')
const { deleteStudentListsTable } = require('./studentLists')
const { deleteGroupsTable } = require('./groups')
const { deleteAttendanceTable } = require('./attendance')
const { deleteUsersGroupsTable } = require('./usersGroups')
const { deleteComipemsTable } = require('./comipems')
const { deleteSchedulesCatalog } = require('./schedulesCatalog')

async function deleteTables () {
  await deleteLocationsTable()
  await deleteClassRoomsTable()
  await deleteUsersTable()
  await deleteSchedulesTable()
  await deleteCoursesTable()
  await deleteStudentListsTable()
  await deleteGroupsTable()
  await deleteAttendanceTable()
  await deleteUsersGroupsTable()
  await deleteComipemsTable()
  await deleteSchedulesCatalog()
}

deleteTables()
