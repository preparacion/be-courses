/**
 * ddb/studentLists.js
 *
 * @description :: Describes the studentLists db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const tableName = 'studentLists'

const studentListsSchema = {
  TableName: tableName,
  KeySchema: [
    { AttributeName: 'studentListId', KeyType: 'HASH' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'studentListId', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 20,
    WriteCapacityUnits: 20
  }
}

const createStudentListsTable = async () => {
  const createTablePromise = ddbClient.createTable(studentListsSchema).promise()
  createTablePromise
    .then(result => {
      console.log('+ StudentLists table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create StudentLists table.', JSON.stringify(err, null, 2))
    })
}

const deleteStudentListsTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: tableName }).promise()
  deleteTablePromise
    .then(result => {
      console.log('+ StudentLists table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete StudentLists table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createStudentListsTable, deleteStudentListsTable }
