/**
 * ddb/comipems.js
 *
 * @description :: Describes the comipems db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const tableName = 'comipems'

const comipemsSchema = {
  TableName: tableName,
  KeySchema: [
    { AttributeName: 'comipemsId', KeyType: 'HASH' },
  ],
  AttributeDefinitions: [
    { AttributeName: 'comipemsId', AttributeType: 'S' },
    { AttributeName: 'courseId', AttributeType: 'S' },
    { AttributeName: 'groupId', AttributeType: 'S' },
    { AttributeName: 'userId', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 20,
    WriteCapacityUnits: 20
  },
  GlobalSecondaryIndexes: [
    {
      IndexName: 'CourseIndex',
      KeySchema: [
        { AttributeName: 'courseId', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    },
    {
      IndexName: 'GroupIndex',
      KeySchema: [
        { AttributeName: 'groupId', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    },
    {
      IndexName: 'UserIndex',
      KeySchema: [
        { AttributeName: 'userId', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    }
  ]
}

const createComipemsTable = async () => {
  const createTablePromise = ddbClient.createTable(comipemsSchema).promise()
  createTablePromise
    .then(result => {
      console.log('+ Compiems table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create Compiems table.', JSON.stringify(err, null, 2))
    })
}

const deleteComipemsTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: tableName }).promise()
  deleteTablePromise
    .then(result => {
      console.log('+ Compiems table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete Compiems table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createComipemsTable, deleteComipemsTable }
