/**
 * ddb/courses.js
 *
 * @description :: Describes the courses db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const tableName = 'courses'

const coursesSchema = {
  TableName: tableName,
  KeySchema: [
    { AttributeName: 'courseId', KeyType: 'HASH' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'courseId', AttributeType: 'S' },
    { AttributeName: 'type', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 20,
    WriteCapacityUnits: 20
  },
  GlobalSecondaryIndexes: [
    {
      IndexName: 'TypeIndex',
      KeySchema: [
        { AttributeName: 'type', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    }
  ]
}

const createCoursesTable = async () => {
  const createTablePromise = ddbClient.createTable(coursesSchema).promise()
  createTablePromise
    .then(result => {
      console.log('+ Courses table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create Courses table.', JSON.stringify(err, null, 2))
    })
}

const deleteCoursesTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: tableName }).promise()
  deleteTablePromise
    .then(result => {
      console.log('+ Courses table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete Courses table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createCoursesTable, deleteCoursesTable }
