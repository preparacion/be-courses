/**
 * ddb/attendance.js
 *
 * @description :: Describes the attendance db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const tableName = 'attendance'

const attendanceSchema = {
  TableName: tableName,
  KeySchema: [
    { AttributeName: 'attendanceId', KeyType: 'HASH' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'attendanceId', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 5,
    WriteCapacityUnits: 5
  }
}

const createAttendanceTable = async () => {
  const createTablePromise = ddbClient.createTable(attendanceSchema).promise()
  createTablePromise
    .then(result => {
      console.log('+ Attendance table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create Attendance table.', JSON.stringify(err, null, 2))
    })
}

const deleteAttendanceTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: tableName }).promise()
  deleteTablePromise
    .then(result => {
      console.log('+ Attendance table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete Attendance table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createAttendanceTable, deleteAttendanceTable }
