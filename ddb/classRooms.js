/**
 * ddb/classRooms.js
 *
 * @description :: Describes the classRooms db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const tableName = 'classrooms'

const classRoomsSchema = {
  TableName: tableName,
  KeySchema: [
    { AttributeName: 'classRoomId', KeyType: 'HASH' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'classRoomId', AttributeType: 'S' },
    { AttributeName: 'locationId', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 5,
    WriteCapacityUnits: 5
  },
  GlobalSecondaryIndexes: [
    {
      IndexName: 'LocationIndex',
      KeySchema: [
        { AttributeName: 'locationId', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 3,
        WriteCapacityUnits: 3
      }
    }
  ]
}

const createClassRoomsTable = async () => {
  const createTablePromise = ddbClient.createTable(classRoomsSchema).promise()
  createTablePromise
    .then(result => {
      console.log('+ ClassRooms table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create Classrooms table.', JSON.stringify(err, null, 2))
    })
}
const deleteClassRoomsTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: tableName }).promise()
  deleteTablePromise
    .then(result => {
      console.log('+ ClassRooms table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete ClassRooms table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createClassRoomsTable, deleteClassRoomsTable }
