/**
 * ddb/usersGroups.js
 *
 * @description :: Describes the usersGroups db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const tableName = 'usersGroups'

const usersGroupsSchema = {
  TableName: tableName,
  KeySchema: [
    { AttributeName: 'usersGroupsId', KeyType: 'HASH' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'usersGroupsId', AttributeType: 'S' },
    { AttributeName: 'userId', AttributeType: 'S' },
    { AttributeName: 'groupId', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 20,
    WriteCapacityUnits: 20
  },
  GlobalSecondaryIndexes: [
    {
      IndexName: 'UserIndex',
      KeySchema: [
        { AttributeName: 'userId', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    },
    {
      IndexName: 'GroupIndex',
      KeySchema: [
        { AttributeName: 'groupId', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    }
  ]
}

const createUsersGroupsTable = async () => {
  const createTablePromise = ddbClient.createTable(usersGroupsSchema).promise()
  createTablePromise
    .then(result => {
      console.log('+ UsersGroups table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create UsersGroups table.', JSON.stringify(err, null, 2))
    })
}

const deleteUsersGroupsTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: tableName }).promise()
  deleteTablePromise
    .then(result => {
      console.log('+ UsersGroups table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete UsersGroups table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createUsersGroupsTable, deleteUsersGroupsTable }
