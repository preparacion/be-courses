/**
 * ddb/createTables.js
 *
 * @description :: Creates tables
 * @docs        :: TODO
 */

const { createLocationsTable } = require('./locations')
const { createClassRoomsTable } = require('./classRooms')
const { createUsersTable } = require('./users')
const { createSchedulesTable } = require('./schedules')
const { createCoursesTable } = require('./courses')
const { createStudentListsTable } = require('./studentLists')
const { createGroupsTable } = require('./groups')
const { createAttendanceTable } = require('./attendance')
const { createUsersGroupsTable } = require('./usersGroups')
const { createComipemsTable } = require('./comipems')
const { createSchedulesCatalog } = require('./schedulesCatalog')

async function createTables () {
  await createLocationsTable()
  await createClassRoomsTable()
  await createUsersTable()
  await createSchedulesTable()
  await createCoursesTable()
  await createStudentListsTable()
  await createGroupsTable()
  await createAttendanceTable()
  await createUsersGroupsTable()
  await createComipemsTable()
  await createSchedulesCatalog()
}

createTables()
