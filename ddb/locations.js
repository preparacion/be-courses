/**
 * ddb/locations.js
 *
 * @description :: Describes the locations db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const locationsSchema = {
  TableName: 'locations',
  KeySchema: [
    { AttributeName: 'locationId', KeyType: 'HASH' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'locationId', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 20,
    WriteCapacityUnits: 20
  }
}

const createLocationsTable = async () => {
  const createTablePromise = ddbClient.createTable(locationsSchema).promise()
  createTablePromise
    .then(result => {
      console.log('+ Locations table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create locations table.', JSON.stringify(err, null, 2))
    })
}

const deleteLocationsTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: 'locations' }).promise()
  deleteTablePromise
    .then(result => {
      console.log('+ Locations table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete locations table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createLocationsTable, deleteLocationsTable }
