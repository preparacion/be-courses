/**
 * ddb/schedules.js
 *
 * @description :: Describes the schedules db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const tableName = 'schedules'

const schedulesSchema = {
  TableName: tableName,
  KeySchema: [
    { AttributeName: 'scheduleId', KeyType: 'HASH' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'scheduleId', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 20,
    WriteCapacityUnits: 20
  }
}

const createSchedulesTable = async () => {
  const createTablePromise = ddbClient.createTable(schedulesSchema).promise()
  createTablePromise
    .then(result => {
      console.log('+ Schedules table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create schedules table.', JSON.stringify(err, null, 2))
    })
}

const deleteSchedulesTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: tableName }).promise()
  deleteTablePromise
    .then(result => {
      console.log('+ Schedules table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete Schedules table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createSchedulesTable, deleteSchedulesTable }
