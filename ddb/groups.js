/**
 * ddb/groups.js
 *
 * @description :: Describes the groups db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const tableName = 'groups'

const groupsSchema = {
  TableName: tableName,
  KeySchema: [
    { AttributeName: 'groupId', KeyType: 'HASH' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'groupId', AttributeType: 'S' },
    { AttributeName: 'name', AttributeType: 'S' },
    { AttributeName: 'courseId', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 20,
    WriteCapacityUnits: 20
  },
  GlobalSecondaryIndexes: [
    {
      IndexName: 'NameIndex',
      KeySchema: [
        { AttributeName: 'name', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    },
    {
      IndexName: 'CourseIndex',
      KeySchema: [
        { AttributeName: 'courseId', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 3,
        WriteCapacityUnits: 3
      }
    }
  ]
}

const createGroupsTable = async () => {
  const createTablePromise = ddbClient.createTable(groupsSchema).promise()
  createTablePromise
    .then(result => {
      console.log('+ Groups table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create Groups table.', JSON.stringify(err, null, 2))
    })
}

const deleteGroupsTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: tableName }).promise()
  deleteTablePromise
    .then(result => {
      console.log('+ Groups table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete Groups table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createGroupsTable, deleteGroupsTable }
