/**
 * ddb/schedulesCatalog.js
 *
 * @description :: Describes the schedulesCatalog db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const schedulesCatalogSchema = {
  TableName: 'schedulesCatalog',
  KeySchema: [
    { AttributeName: 'schedulesCatalogId', KeyType: 'HASH' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'schedulesCatalogId', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 10,
    WriteCapacityUnits: 10
  }
}

const createSchedulesCatalog = async () => {
  const createTablePromise = ddbClient.createTable(schedulesCatalogSchema).promise()
  createTablePromise
    .then(result => {
      console.log('+ SchedulesCatalogSchema table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create schedulesCatalog table.', JSON.stringify(err, null, 2))
    })
}

const deleteSchedulesCatalog = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: 'schedulesCatalog' }).promise()
  deleteTablePromise
    .then(result => {
      console.log('+ SchedulesCatalogSchema table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete schedulesCatalog table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createSchedulesCatalog, deleteSchedulesCatalog }
