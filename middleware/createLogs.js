/**
 * middleware/logs.js
 *
 * @description :: Middleware for create logs in db
 * @docs        :: TODO
 */
const _ = require('underscore')
const uuidv4 = require('uuid/v4')

const { docClient } = require('../ddb/client')

const tableName = 'logs'

module.exports = async (ctx, next) => {
  const id = uuidv4()
  let params = {
    TableName: tableName,
    Item: {
      logId: id
    }
  }

  const data = {
    url: ctx.url || null,
    method: ctx.method || null
  }

  if (ctx.state && ctx.state.user) {
    data['date'] = new Date().toISOString()
    data['userId'] = ctx.state.user.userId
    data['action'] = ctx.state.action || 'none'
    data['data'] = JSON.stringify(ctx.state.data, null, 2) || JSON.stringify('{}', null, 2)

    params.Item = _.extend(params.Item, data)

    const createLog = docClient.put(params).promise()
    await createLog
      .then()
      .catch(err => {
        console.error('- Error trying to create a new log.', JSON.stringify(err, null, 2))
      })
  }
}
