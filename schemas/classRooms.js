/**
 * schemas/classRooms.js
 *
 * @description :: Defines classRooms validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const classRoomSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100).required(),
  capacity: Joi.number().integer().required(),
  locationId: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/)
})

const updateClassRoomSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100),
  capacity: Joi.number().integer(),
  locationId: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/)
})

module.exports = {
  classRoomSchema,
  updateClassRoomSchema
}
