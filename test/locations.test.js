/**
 * tests/integration/locations.test.js
 *
 * @description :: Runs locations tests
 * @docs        ::
 */
const chai = require('chai')
const faker = require('faker')
const uuidv4 = require('uuid/v4')
const { port } = require('../config')
const expect = require('chai').expect
const chaiHttp = require('chai-http')

const Locations = require('../models/locations')

const host = 'http://localhost'

chai.use(chaiHttp)

describe('locations functions', () => {
  const uuid = uuidv4()
  describe('get', () => {
    it('get is a function', done => {
      expect(Locations.get).to.be.a('function')
      done()
    })

    it('returns an array of locations', done => {
      Promise.resolve(Locations.get())
        .then(result => {
          expect(result).to.be.a('object')
          expect(result).to.have.property('success')
          expect(result).to.have.property('locations')
          expect(result.success).to.equal(true)
          expect(result.locations).to.be.a('array')
          done()
        })
        .catch(err => {
          console.log(`Error in locations tests ${err}`)
          done()
        })
    })
  })

  describe('create', () => {
    it('create is a function', done => {
      expect(Locations.create).to.be.a('function')
      done()
    })

    it('create a new location', done => {
      const data = {
        locationId: uuid,
        latitude: faker.address.latitude(),
        longitude: faker.address.longitude()
      }

      Promise.resolve(Locations.create(data))
        .then(result => {
          expect(result).to.be.a('object')
          expect(result).to.have.property('success')
          expect(result).to.have.property('locationId')
          expect(result.success).to.equal(true)
          expect(result.locationId).to.be.a('string')
          done()
        })
        .catch(err => {
          console.log(`Error in locations tests ${err}`)
          done()
        })
    })
  }).timeout(15000)

  describe('getById', () => {
    it('getById is a function', done => {
      expect(Locations.getById).to.be.a('function')
      done()
    })

    it('return a location', done => {
      Promise.resolve(Locations.getById(uuid))
        .then(result => {
          expect(result).to.be.a('object')
          expect(result).to.have.property('success')
          expect(result).to.have.property('location')
          expect(result.success).to.equal(true)
          expect(result.location).to.be.a('object')
          done()
        })
        .catch(err => {
          console.log(`Error in locations tests ${err}`)
          done()
        })
    })
  })

  describe('delete', () => {
    it('delete is a function', done => {
      expect(Locations.delete).to.be.a('function')
      done()
    })

    it('delete an item', done => {
      Promise.resolve(Locations.delete(uuid))
        .then(result => {
          expect(result).to.be.a('object')
          expect(result).to.have.property('success')
          expect(result).to.have.property('message')
          expect(result.success).to.equal(true)
          expect(result.message).to.be.a('string')
          done()
        })
        .catch(err => {
          console.log(`Error in locations tests ${err}`)
          done()
        })
    })
  })
})

describe('locations routes -', () => {
  const route = '/api/locations'
  const uuid = uuidv4()

  it('return all locations', done => {
    chai.request(`${host}:${port}`)
      .get(route)
      .then(res => {
        expect(res).to.have.status(200)
        expect(res.body).to.have.property('success')
        expect(res.body).to.have.property('locations')
        expect(res.body.locations).to.be.a('array')
        done()
      })
      .catch(err => {
        console.log(`Error in locations tests ${err}`)
        done()
      })
  })

  it('create a new location', done => {
    const data = {
      locationId: uuid,
      latitude: faker.address.latitude(),
      longitude: faker.address.longitude()
    }
    chai.request(`${host}:${port}`)
      .post(route)
      .send(data)
      .then(res => {
        expect(res).to.have.status(201)
        expect(res.body).to.have.property('success')
        expect(res.body).to.have.property('locationId')
        expect(res.body.locationId).to.be.a('string')
        done()
      })
      .catch(err => {
        console.log(`Error in locations tests ${err}`)
        done()
      })
  })

  it('get an item by id', done => {
    chai.request(`${host}:${port}`)
      .get(`${route}/${uuid}`)
      .then(res => {
        expect(res).to.have.status(200)
        expect(res.body).to.have.property('success')
        expect(res.body).to.have.property('location')
        expect(res.body.location).to.be.a('object')
        done()
      })
      .catch(err => {
        console.log(`Error in locations tests ${err}`)
        done()
      })
  })

  it('delete an item', done => {
    chai.request(`${host}:${port}`)
      .delete(`${route}/${uuid}`)
      .then(res => {
        expect(res).to.have.status(200)
        expect(res.body).to.have.property('success')
        expect(res.body).to.have.property('message')
        expect(res.body.message).to.be.a('string')
        done()
      })
      .catch(err => {
        console.log(`Error in locations tests ${err}`)
        done()
      })
  })
})
