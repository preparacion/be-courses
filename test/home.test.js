/**
 * tests/integration/home.test.js
 *
 * @description :: Runs tests
 * @docs        ::
 */
const chai = require('chai')
const { port } = require('../config')
const expect = require('chai').expect
const chaiHttp = require('chai-http')

const app = require('../server')

const host = 'http://localhost'

chai.use(chaiHttp)

describe('foo', () => {
  it('respond with 200', done => {
    chai.request(`${host}:${port}`)
      .get('/')
      .then(res => {
        expect(res).to.have.status(200)
        done()
      })
      .catch(err => {
        throw err
      })
  })
})
