/**
 * routes/index.js
 *
 * @description :: Defines routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

const isAuth = require('../middleware/isAuth')

// Home routes
const homeApi = require('./api/home')
router.use('', homeApi.routes(), homeApi.allowedMethods())
// Locations routes
const locationsApi = require('./api/locations')
router.use(isAuth, locationsApi.routes(), locationsApi.allowedMethods())
// Classrooms routes
const classRoomsApi = require('./api/classRooms')
router.use('', classRoomsApi.routes(), classRoomsApi.allowedMethods())
// Schedules routes
const schedulesApi = require('./api/schedules')
router.use('', schedulesApi.routes(), schedulesApi.allowedMethods())
// Courses routes
const coursesApi = require('./api/courses')
router.use('', coursesApi.routes(), coursesApi.allowedMethods())
// Attendance routes
const attendanceApi = require('./api/attendance')
router.use('', attendanceApi.routes(), attendanceApi.allowedMethods())
// StudentLists routes
const studentListsApi = require('./api/studentLists')
router.use('', studentListsApi.routes(), studentListsApi.allowedMethods())
// Groups routes
const groupsApi = require('./api/groups')
router.use('', groupsApi.routes(), groupsApi.allowedMethods())
// UsersGroups routes
const usersGroupsApi = require('./api/usersGroups')
router.use('', usersGroupsApi.routes(), usersGroupsApi.allowedMethods())
// schedulesCatalog routes
const schedulesCatalogApi = require('./api/schedulesCatalog')
router.use('', schedulesCatalogApi.routes(),
  schedulesCatalogApi.allowedMethods())

module.exports = router
