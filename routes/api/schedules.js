/**
 * router/api/schedules.js
 *
 * @description :: Describes the schedules api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

const Schedules = require('../../models/schedules')

router.prefix('/api/schedules')

// returns all the schedules
router.get('/', async (ctx, next) => {
  const result = await Schedules.get()
  if (result.success) {
    ctx.state.action = `get all the schedules`
    ctx.state.data = {}

    ctx.body = {
      success: true,
      locations: result.locations
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Schedules.getById(id)

  if (result.success) {
    ctx.state.action = `get a schedule ${id}`
    ctx.state.data = { id }

    ctx.body = {
      success: true,
      location: result.schedule
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Create an item
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const result = await Schedules.create(body)

  if (result.success) {
    ctx.state.action = `create a schedyle`
    ctx.state.data = body

    ctx.status = 201
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Delete an item
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Schedules.delete(id)

  if (result.success) {
    ctx.state.action = `delete a schedule ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
