/**
 * router/api/locations.js
 *
 * @description :: Describes the locations api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const Locations = require('../../models/locations')
router.prefix('/api/locations')

const createLocationSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100).required(),
  opening: Joi.string().min(1).max(50).required(),
  closing: Joi.string().min(1).max(50).required(),
  street: Joi.string().min(1).max(100),
  postalCode: Joi.number().integer(),
  district: Joi.string().min(1).max(100),
  latitude: Joi.string(),
  longitude: Joi.string()
})

const updateLocationSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100),
  opening: Joi.string().min(1).max(50),
  closing: Joi.string().min(1).max(50),
  street: Joi.string().min(1).max(100),
  postalCode: Joi.number().integer(),
  district: Joi.string().min(1).max(100),
  latitude: Joi.string(),
  longitude: Joi.string()
})

router.get('/', async (ctx, next) => {
  const result = await Locations.get()
  if (result.success) {
    ctx.state.action = `get all locations`
    ctx.state.data = {}

    ctx.body = {
      success: true,
      locations: result.locations
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Locations.getById(id)

  if (result.success) {
    ctx.body = {
      success: true,
      location: result.location
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Create an item
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, createLocationSchema)

  if (validationResult.error === null) {
    const result = await Locations.create(body)

    if (result.success) {
      ctx.state.action = `create a group`
      ctx.state.data = body

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Updates a single item by id
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, updateLocationSchema)

  if (validationResult.error === null) {
    const result = await Locations.update(id, validationResult.value)

    if (result.success) {
      ctx.state.action = `update a group ${id}`
      ctx.state.data = { id }

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Locations.delete(id)

  if (result.success) {
    ctx.state.action = `delete a group ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
