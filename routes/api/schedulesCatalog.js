/**
 * router/api/schedulesCatalog.js
 *
 * @description :: Describes the schedulesCatalog api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const SchedulesCatalog = require('../../models/schedulesCatalog')
router.prefix('/api/schedules_catalog')

// schedulesCatalog validation schemas
const createScheduleSchema = Joi.object()
  .keys({
    scheduleAlias: Joi.string().min(1).max(100).required(),
    start: Joi.string().min(1).max(4),
    end: Joi.string().min(1).max(4)
  })

const updateScheduleSchema = Joi.object().keys({
  scheduleAlias: Joi.string().min(1).max(100),
  start: Joi.string().min(1).max(4),
  end: Joi.string().min(1).max(4)
})

router.get('/', async (ctx, next) => {
  const result = await SchedulesCatalog.get()
  if (result.success) {
    ctx.state.action = `get all schedules - catalog`
    ctx.state.data = {}

    ctx.body = {
      success: true,
      schedules: result.schedules
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await SchedulesCatalog.getById(id)

  if (result.success) {
    ctx.state.action = `get a schedule - catalog ${id}`
    ctx.state.data = { id }

    ctx.body = {
      success: true,
      schedule: result.schedule
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Create an item
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, createScheduleSchema)

  if (validationResult.error === null) {
    const result = await SchedulesCatalog.create(validationResult.value)

    if (result.success) {
      ctx.state.action = `create a schedule in catalog`
      ctx.state.data = validationResult.value

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, updateScheduleSchema)

  if (validationResult.error === null) {
    const result = await SchedulesCatalog.update(id, validationResult.value)

    if (result.success) {
      ctx.state.action = `update a schedule - catalog ${id}`
      ctx.state.data = { id }

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await SchedulesCatalog.delete(id)

  if (result.success) {
    ctx.state.action = `delete a schedule - catalog ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
