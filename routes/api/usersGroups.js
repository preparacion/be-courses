/**
 * router/api/usersGroups.js
 *
 * @description :: Describes the usersGroups api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const UsersGroups = require('../../models/usersGroups')

router.prefix('/api/usersgroups')

const createUsersGroupsSchema = Joi.object().keys({
  userId: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/)
    .required(),
  groupId: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/)
    .required()
})

const paginationSchema = Joi.object().keys({
  limit: Joi.number().integer().max(1000).default(0),
  skip: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/)
    .default(null)
})

const updateUserGroupSchema = Joi.object().keys({
  userId: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/),
  groupId: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/)
})

router.get('/', async (ctx, next) => {
  const query = ctx.query || {}

  const validationResult = Joi.validate(query, paginationSchema)

  if (validationResult.error === null) {
    const result = await UsersGroups.get(validationResult.value)

    if (result.success) {
      ctx.state.action = `get all the usersgroups`
      ctx.state.data = {}

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await UsersGroups.getById(id)

  if (result.success) {
    ctx.state.action = `get an userGroup ${id}`
    ctx.state.data = { id }

    ctx.body = {
      success: true,
      userGroup: result.userGroup
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Create a new item
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, createUsersGroupsSchema)

  if (validationResult.error === null) {
    const result = await UsersGroups.create(body)

    if (result.success) {
      ctx.state.action = `create a new usergroup`
      ctx.state.data = body

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Update an item
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, updateUserGroupSchema)

  if (validationResult.error === null) {
    const result = await UsersGroups.update(id, validationResult.value)

    if (result.success) {
      ctx.state.action = `update a usergroup ${id}`
      ctx.state.data = { id }

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Removes an item
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await UsersGroups.delete(id)

  if (result.success) {
    ctx.state.action = `delete an usergroup ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
