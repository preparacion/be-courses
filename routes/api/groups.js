/**
 * router/api/groups.js
 *
 * @description :: Describes the groups api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const Groups = require('../../models/groups')

router.prefix('/api/groups')

// Groups Validation Schemas
const createGroupSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100).required(),
  startDate: Joi.date().required(),
  active: Joi.boolean().default(false),
  capacity: Joi.number().integer().min(1).max(100).default(1),
  courseId: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/),
  teacherId: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/),
  scheduleId: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/),
  classRoomId: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/),
  locationId: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/),
  studentList: Joi.array().items(
    Joi.string()
      .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/)
  )
})

const updateGroupSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100),
  startDate: Joi.date(),
  active: Joi.boolean(),
  quota: Joi.number().integer().min(1).max(100),
  courseId: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/),
  teacherId: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/),
  scheduleId: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/),
  classRoomId: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/),
  locationId: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/),
  studentList: Joi.array().items(
    Joi.string()
      .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/)
  )
})

const paginationSchema = Joi.object().keys({
  limit: Joi.number().integer().max(1000).default(0),
  skip: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/)
    .default(null)
})

// Get all the items
router.get('/', async (ctx, next) => {
  const query = ctx.query || {}

  const validationResult = Joi.validate(query, paginationSchema)

  if (validationResult.error === null) {
    const result = await Groups.get(validationResult.value)

    if (result.success) {
      ctx.state.action = `get all the groups`
      ctx.state.data = {}

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Groups.getById(id)

  if (result.success) {
    ctx.state.action = `get a group ${id}`
    ctx.state.data = { id }

    ctx.body = {
      success: true,
      group: result.group
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Creates a new item
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}
  body.startDate = new Date(body.startDate).toISOString()

  const validationResult = Joi.validate(body, createGroupSchema)

  if (validationResult.error === null) {
    const result = await Groups.create(body)

    if (result.success) {
      ctx.state.action = `create a group`
      ctx.state.data = body

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Updates a single item by id
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, updateGroupSchema)

  if (validationResult.error === null) {
    if (validationResult.value.startDate) {
      validationResult.value.startDate = new Date(validationResult.value.startDate)
        .toISOString()
    }

    const result = await Groups.update(id, validationResult.value)

    if (result.success) {
      ctx.state.action = `update a group ${id}`
      ctx.state.data = { id }

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

router.put('/:id/enroll/:studentId', async (ctx, next) => {
  const id = ctx.params.id
  const studentId = ctx.params.studentId
  const body = ctx.request.body || {}
  // logged user
  const user = ctx.state.user

  const result = await Groups.enroll(id, studentId, body, user)
  ctx.body = result
})

// Removes a single item by id
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Groups.delete(id)

  if (result.success) {
    ctx.state.action = `delete a group ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
