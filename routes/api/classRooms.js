/**
 * router/api/classRooms.js
 *
 * @description :: Describes the classRooms api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const ClassRooms = require('../../models/classRooms')

router.prefix('/api/classrooms')

const {
  classRoomSchema,
  updateClassRoomSchema } = require('../../schemas/classRooms')

// returns all the class rooms
router.get('/', async (ctx, next) => {
  const result = await ClassRooms.get()

  if (result.success) {
    ctx.state.action = `Get all the classrooms`
    ctx.state.data = {}

    ctx.body = {
      success: true,
      classRooms: result.classRooms
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await ClassRooms.getById(id)

  if (result.success) {
    ctx.state.action = `get a classrooms ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Create a new classroom
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, classRoomSchema)

  if (validationResult.error === null) {
    const result = await ClassRooms.create(validationResult.value)

    if (result.success) {
      ctx.state.action = `create a classroom`
      ctx.state.data = {}

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, updateClassRoomSchema)

  if (validationResult.error === null) {
    const result = await ClassRooms.update(id, validationResult.value)

    if (result.success) {
      ctx.state.action = `Update a classroom ${id}`
      ctx.state.data = validationResult.value

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

router.get('/location/:id', async (ctx, next) => {
  const id = ctx.params.id
  const result = await ClassRooms.getByLocationId(id)

  if (result.success) {
    ctx.state.action = `get classrooms by location id ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  ctx.state.action = `delete a classroom ${id}`
  ctx.state.data = { id }

  ctx.body = {
    success: true,
    message: `Deleted ${id}`
  }

  return next()
})

module.exports = router
