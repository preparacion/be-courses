/**
 * router/api/home.js
 *
 * @description :: Describes the home api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

router.get('/', async (ctx) => {
  ctx.body = {
    success: true,
    message: 'be-courses, ok'
  }
})

module.exports = router
