/**
 * router/api/courses.js
 *
 * @description :: Describes the courses api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const Courses = require('../../models/courses')
router.prefix('/api/courses')

// courses validation schemas
const createCourseSchema = Joi.object()
  .keys({
    name: Joi.string().min(1).max(100).required(),
    duration: Joi.number().integer().min(1).max(1000).required(),
    creationDate: Joi.date().default(new Date().toISOString()),
    active: Joi.bool().default(false),
    cost: Joi.number().positive(),
    kind: Joi.string().min(1).max(100),
    type: Joi.string().min(1).max(100)
  })

const updateCourseSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100),
  duration: Joi.number().integer().min(1).max(100),
  active: Joi.boolean(),
  cost: Joi.number().positive(),
  kind: Joi.string().min(1).max(100),
  type: Joi.string().min(1).max(100)
})

const paginationSchema = Joi.object().keys({
  limit: Joi.number().integer().max(1000).default(0),
  skip: Joi.number().integer().max(1000)
})

router.get('/', async (ctx, next) => {
  const query = ctx.query || {}

  const validationResult = Joi.validate(query, paginationSchema)

  if (validationResult.error === null) {
    const result = await Courses.get(validationResult.value)

    if (result.success) {
      ctx.state.action = `get all the courses`
      ctx.state.data = {}

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Courses.getById(id)

  if (result.success) {
    ctx.state.action = `get a course ${id}`
    ctx.state.data = { id }

    ctx.body = {
      success: true,
      location: result.course
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Create an item
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, createCourseSchema)

  if (validationResult.error === null) {
    const result = await Courses.create(validationResult.value)

    if (result.success) {
      ctx.state.action = `create a course`
      ctx.state.data = validationResult.value

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, updateCourseSchema)

  if (validationResult.error === null) {
    const result = await Courses.update(id, validationResult.value)

    if (result.success) {
      ctx.state.action = `update a course ${id}`
      ctx.state.data = { id }

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Courses.delete(id)

  if (result.success) {
    ctx.state.action = `delete a course ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// level = type
router.get('/level/:level', async (ctx, next) => {
  const level = ctx.params.level

  const result = await Courses.getByType(level)

  if (result.success) {
    ctx.state.action = `get courses by type ${level}`
    ctx.state.data = { level }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// get all groups associated to a course
router.get('/:id/groups', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Courses.getGroups(id)

  if (result.success) {
    ctx.state.action = `get courses by type ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

router.get('/dependency/:dependency', async (ctx, next) => {
  const dependency = ctx.params.dependency

  const result = await Courses.getByDependency(dependency)

  if (result.success) {
    ctx.state.action = `get courses by dependency ${dependency}`
    ctx.state.data = { dependency }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
