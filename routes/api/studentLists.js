/**
 * router/api/studentLists.js
 *
 * @description :: Describes the studentLists api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

const StudentLists = require('../../models/studentLists')
router.prefix('/api/studentLists')

router.get('/', async (ctx, next) => {
  const result = await StudentLists.get()

  if (result.success) {
    ctx.state.action = `get all studentlists`
    ctx.state.data = {}
    ctx.body = {
      success: true,
      studentLists: result.studentLists
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await StudentLists.getById(id)

  if (result.success) {
    ctx.state.action = `get a studentlist ${id}`
    ctx.state.data = { id }

    ctx.body = {
      success: true,
      studentList: result.studentList
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Create an item
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const result = await StudentLists.create(body)

  if (result.success) {
    ctx.state.action = `create a new studentlist`
    ctx.state.data = body

    ctx.status = 201
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Delete an item
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await StudentLists.delete(id)

  if (result.success) {
    ctx.state.action = `delete a studentlist ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
