/**
 * router/api/attendance.js
 *
 * @description :: Describes the attendance api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

const Attendance = require('../../models/attendance')
router.prefix('/api/attendance')

router.get('/', async (ctx, next) => {
  const result = await Attendance.get()

  if (result.success) {
    ctx.state.action = `get all attendance`
    ctx.state.data = {}
    ctx.body = {
      success: true,
      locations: result.attendances
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Attendance.getById(id)

  if (result.success) {
    ctx.state.action = `get an attendance ${id}`
    ctx.state.data = { id }

    ctx.body = {
      success: true,
      location: result.attendance
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Create an item
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const result = await Attendance.create(body)

  if (result.success) {
    ctx.state.action = `create attendance`
    ctx.state.data = body

    ctx.status = 201
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Delete an item
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Attendance.delete(id)

  if (result.success) {
    ctx.state.action = `delete an attendance ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
