/**
 * loadCourses.js
 */
const fs = require('fs')
const util = require('util')
const _ = require('underscore')

// const Courses = require('../models/courses')
const Groups = require('../models/groups')
const Courses = require('../models/courses')
const Schedules = require('../models/schedules')
const ClassRooms = require('../models/classRooms')
const StudentLists = require('../models/studentLists')

const readFile = util.promisify(fs.readFile)

const file = './old_data/grupos.csv'
const dataFile = fs.createWriteStream('./data/grupos.csv', { autoClose: true })

const getGroupsLines = async function (path) {
  const dataFile = await readFile(path, 'utf-8')
  let lines = dataFile.split(/\n/)
  let header = lines.shift().replace(/\r/, '').split(',')
  header[0] = 'old_id'

  return {
    lines: lines.map(line => line.replace(/\r/, '')),
    header
  }
}

const lineToJson = function (line) {
  const course = line.split(',')
  const dateArray = course[2].split('-')
  const day = dateArray[0]
  const year = `20${dateArray[2]}`
  const month = (dateArray[1] === 'ene') ? '0'
    : (dateArray[1] === 'oct') ? '9'
      : (dateArray[1] === 'nov') ? '10' : ''

  let courseData = {
    oldId: course[0],
    name: course[1],
    startDate: new Date(year, month, day),
    active: course[3],
    quota: course[4],
    oldCourseId: course[5],
    oldScheduleId: course[8],
    oldClassRoomId: course[10]
  }

  for (const i in courseData) {
    if (courseData[i] === '') {
      delete courseData[i]
    }
  }

  return courseData
}

const saveGroup = async function (line) {
  let courseData = lineToJson(line)

  const courseResult = await Courses.getByOldId(courseData.oldCourseId)
  if (courseResult.success) {
    courseData = _.extend(courseData, { courseId: courseResult.course._id })
  }

  const scheduleResult = await Schedules.getByOldId(courseData.oldScheduleId)
  if (scheduleResult.success) {
    courseData = _.extend(courseData, { scheduleId: scheduleResult.schedule._id })
  }

  const classRoomResult = await ClassRooms.getByOldId(courseData.oldClassRoomId)
  if (classRoomResult.success) {
    courseData = _.extend(courseData, { classRoomId: classRoomResult.classRoom._id })
  }

  const studentListResult = await StudentLists.create({ name: courseData.name, list: [] })
  courseData = _.extend(courseData, { studentListId: studentListResult.studentList._id })

  const result = await Groups.create(courseData)
  const id = result.group._id

  dataFile.write(id + ',' +
   line +
    '\n'
  )
}

const init = async function () {
  const data = await getGroupsLines(file)
  // save header
  dataFile.write('id,' + data.header + '\n')

  await Promise.all(data.lines.map(line => saveGroup(line)))
    .then(() => {
      // Promise.resolve(Locations.get())
      //   .then(result => {
      //     console.log(result)
      //   })
    })
    .catch(err => {
      console.error(err)
    })
}

Promise.resolve(init())
  .then(result => {
    // console.log(result)
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })