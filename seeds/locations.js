/**
 * seeds/locations.js
 *
 * @description :: Creates example node seeds/locations.js
 * @docs        :: TODO
 */
const faker = require('faker')

const Locations = require('../models/locations')

const createLocation = async function () {
  const locationData = {
    name: faker.name.jobArea(),
    opening: '0900',
    closing: '1700',
    street: faker.address.secondaryAddress(),
    postalCode: '99999',
    district: faker.address.secondaryAddress(),
    latitude: faker.address.latitude(),
    longitude: faker.address.longitude()
  }

  const location = await Locations.create(locationData)
  return location
}

const createSeeds = async function () {
  await createLocation()
  await createLocation()
}

Promise.resolve(createSeeds())
  .then(() => {
    process.exit(0)
  })
  .catch(err => {
    console.log(err)
    process.exit(1)
  })
