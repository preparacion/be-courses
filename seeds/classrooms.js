/**
 * seeds/classrooms.js
 *
 * @description :: Creates example node seeds/classrooms.js
 * @docs        :: TODO
 */
const faker = require('faker')

const ClassRooms = require('../models/classRooms')
const Locations = require('../models/locations')

const createLocation = async function () {
  const locationData = {
    name: faker.name.jobArea(),
    opening: '0900',
    closing: '1700',
    street: faker.address.secondaryAddress(),
    postalCode: '99999',
    district: faker.address.secondaryAddress(),
    latitude: faker.address.latitude(),
    longitude: faker.address.longitude()
  }

  const location = await Locations.create(locationData)
  return location
}

const createClassRoom = async function (locationId) {
  const classRoomData = {
    name: faker.name.jobArea(),
    capacity: 20,
    locationId: locationId
  }

  const classRoom = await ClassRooms.create(classRoomData)
  return classRoom
}

const createSeeds = async function () {
  const location = await createLocation()

  await createClassRoom(location.locationId)

  await createClassRoom(location.locationId)
}

Promise.resolve(createSeeds())
  .then(() => {
    process.exit(0)
  })
  .catch(err => {
    console.log(err)
    process.exit(1)
  })
