/**
 * loadGroups.js
 */
const fs = require('fs')
const _ = require('underscore')
const Promise = require('bluebird')

const StudentLists = require('../models/studentLists')
const Groups = require('../models/groups')

const dataFile = fs.createWriteStream('./data/gruposespeciales.csv', { autoClose: true })

const specialGroups = [
  {
    oldId: 1001,
    name: 'Mate',
    active: false,
    special: true
  },
  {
    oldId: 2002,
    name: 'Física',
    active: false,
    special: true
  },
  {
    oldId: 2003,
    name: 'Química',
    active: false,
    special: true
  },
  {
    oldId: 3003,
    name: 'Cálculo',
    active: false,
    special: true
  }
]

const header = [
  'oldId',
  'nombre',
  'activo',
  'especial'
]

const saveGroup = async function (data) {
  const studentList = await StudentLists.create({
    name: data.name,
    list: []
  })

  const studentListId = studentList.studentList._id

  const groupData = _.extend(data, { studentListId })

  const result = await Groups.create(groupData)
  const id = result.group._id

  dataFile.write(
    id + ',' +
    data['oldId'] + ',' +
    data['name'] + ',' +
    data['active'] + ',' +
    data['special'] + ',' +
    '\n'
  )
}

const init = async function () {
  const data = specialGroups
  // save header
  dataFile.write('id,' + header + '\n')

  const lines = data.map(line => saveGroup(line))

  await Promise.mapSeries(lines, line => { return line })
    .then(() => {

    })
    .catch(err => {
      console.error(err)
    })
}

Promise.resolve(init())
  .then(result => {
    // console.log(result)
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })