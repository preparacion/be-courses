/**
 * loadClassRooms.js
 */
const fs = require('fs')
const util = require('util')
const _ = require('underscore')

const Locations = require('../models/locations')
const ClassRooms = require('../models/classrooms')

const readFile = util.promisify(fs.readFile)

const file = './old_data/salones.csv'
const dataFile = fs.createWriteStream('./data/salones.csv', { autoClose: true })

const getClassroomsLines = async function (path) {
  const dataFile = await readFile(path, 'utf-8')
  let lines = dataFile.split(/\n/)
  let header = lines.shift().replace(/\r/, '').split(',')
  header[0] = 'old_id'

  return {
    lines: lines.map(line => line.replace(/\r/, '')),
    header
  }
}

const lineToJson = function (line) {
  const classRoom = line.split(',')
  const classRoomData = {
    oldId: classRoom[0],
    name: classRoom[1],
    capacity: classRoom[2],
    oldLocationId: classRoom[3]
  }

  for (const i in classRoomData) {
    if (classRoomData[i] === '') {
      delete classRoomData[i]
    }
  }

  return classRoomData
}

const saveClassRoom = async function (line) {
  let classRoomData = lineToJson(line)

  const locationResult = await Locations.getByOldId(classRoomData.oldLocationId)
  const locationId = locationResult.location._id

  classRoomData = _.extend(classRoomData, { locationId })

  const result = await ClassRooms.create(classRoomData)
  const id = result.classRoom._id

  dataFile.write(id + ',' +
    classRoomData.oldId + ',' +
    classRoomData.name + ',' +
    classRoomData.capacity + ',' +
    locationId + ',' +
    '\n')
}

const init = async function () {
  const data = await getClassroomsLines(file)

  // save header
  dataFile.write('id,' + data.header + '\n')

  await Promise.all(data.lines.map(line => saveClassRoom(line)))
    .then(() => {
    })
    .catch(err => {
      console.error(err)
    })
}

Promise.resolve(init())
  .then(result => {
    // console.log(result)
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
