/**
 * seeds/regsiter.js
 *
 * @description :: Creates example node seeds/registerd.js
 * @docs        :: TODO
 */

const Register = require('../models/register')

const crateUsersAdmin = async function () {
  const userData = {
    name: 'admin1',
    lastmane: 'admin1',
    email: 'admin1@gmail.com',
    password: '123456',
    rol: 'SuperAdmin'
  }

  const result = await Register.create(userData)
  console.log(result)
}

const crateUsersAdmin2 = async function () {
  const userData = {
    name: 'admin2',
    lastmane: 'admin2',
    email: 'admin2@gmail.com',
    password: '123456',
    rol: 'SuperAdmin'
  }

  const result = await Register.create(userData)
  console.log(result)
}

const createSeeds = async function () {
  await crateUsersAdmin()
  await crateUsersAdmin2()
}

Promise.resolve(createSeeds())
  .then(() => {
    process.exit(0)
  })
  .catch(err => {
    console.log(err)
    process.exit(1)
  })
