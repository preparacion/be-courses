/**
 * db/schedules.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const ScheduleSchema = new Schema({
  name: { type: String, required: true },
  day: { type: String },
  oldId: { type: String },
  startHour: { type: String },
  endHour: { type: String }
}, { versionKey: false })

ScheduleSchema.virtual('id').get(() => {
  return this._id
})

const Schedules = db.model('Schedules', ScheduleSchema, 'Schedules')

module.exports = Schedules
