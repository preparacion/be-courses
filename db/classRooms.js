/**
 * db/classRooms.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const ClassRoomSchema = new Schema({
  name: { type: String, required: true },
  capacity: { type: Number },
  oldId: { type: String },
  oldLocationId: { type: String },
  locationId: { type: Schema.Types.ObjectId, ref: 'Locations' }
}, { versionKey: false })

// Indexes
ClassRoomSchema.index({ locationId: 1 }, { sparse: true })

ClassRoomSchema.virtual('id').get(() => {
  return this._id
})

const ClassRooms = db.model('ClassRooms', ClassRoomSchema, 'ClassRooms')

module.exports = ClassRooms
