/**
 * db/deleteTables.js
 *
 * @description :: Deletes collections
 * @docs        :: TODO
 */
const GroupModel = require('./groups')
const CourseModel = require('./courses')
const LocationModel = require('./locations')
const ClassRoomModel = require('./classRooms')
const ScheduleModel = require('./schedules')

const deleteCollections = async () => {
  await LocationModel.remove({})
  await ClassRoomModel.remove({})
  await CourseModel.remove({})
  await GroupModel.remove({})
  await ScheduleModel.remove({})
}

Promise.resolve(deleteCollections())
  .then(() => {
    console.log('Collections removed')
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
