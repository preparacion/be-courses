/**
 * db/groups.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const GroupSchema = new Schema({
  name: { type: String, required: true },
  startDate: { type: Date },
  active: { type: Boolean },
  quota: { type: Number },
  special: { type: Boolean },
  mate: { type: String },
  fisica: { type: String },
  quimica: { type: String },
  calculo: { type: String },
  oldId: { type: String },
  oldCourseId: { type: String },
  oldScheduleId: { type: String },
  oldClassRoomId: { type: String },
  courseId: { type: Schema.Types.ObjectId, ref: 'Courses' },
  scheduleId: { type: Schema.Types.ObjectId, ref: 'Schedules' },
  classRoomId: { type: Schema.Types.ObjectId, ref: 'ClassRooms' },
  studentListId: { type: Schema.Types.ObjectId, ref: 'StudentLists' }
}, { versionKey: false })

GroupSchema.virtual('id').get(() => {
  return this._id
})

const Groups = db.model('Groups', GroupSchema, 'Groups')

module.exports = Groups
