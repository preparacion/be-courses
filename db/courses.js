/**
 * db/courses.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const CourseSchema = new Schema({
  name: { type: String, required: true },
  oldId: { type: String },
  dependency: { type: String },
  duration: { type: String },
  tipo: { type: String },
  price: { type: Number },
  active: { type: Boolean },
  creationDate: { type: Date, default: Date.now },
  type: { type: Number }
}, { versionKey: false })

CourseSchema.virtual('id').get(() => {
  return this._id
})

const Courses = db.model('Courses', CourseSchema, 'Courses')

module.exports = Courses
