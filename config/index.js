/**
 * config/index.js
 *
 * @description :: Loads config values
 * @docs        :: TODO
 */
'use strict'

const dotenv = require('dotenv')
const path = require('path')
const env = process.env.NODE_ENV || 'development'

const envPath = path.resolve(__dirname, `../.envs/${env}`)
dotenv.config({ path: envPath })

const configs = {
  base: {
    env,
    name: process.env.APP_NAME || 'koa-basic-boilerplate',
    host: process.env.APP_HOST || '0.0.0.0',
    port: process.env.APP_PORT || 3100,
    key: process.env.APP_KEY || 'ThisISnotAsafeK3y.',
    jwtKey: process.env.JWT_KEY || 'IbMboughtR3DH4tloL35000.. Integer eget ' +
      'rhoncus ante. Nullam viverra facilisis volutpat. Phasellus ullamcorper ' +
      'sapien vitae mi vehicula, et aliquet ex placerat. Maecenas at maximus ' +
      'libero, in efficitur turpis. Fusce ut eros sed quam '
  },
  production: {
    awsRegion: process.env.AWS_REGION || 'local',
    awsEndPoint: process.env.AWS_END_POINT || 'http://localhost:8000',
    accessKeyId: process.env.AWS_ACCESS_KEY || 'accessID_',
    secretAccessKey: process.env.AWS_SECRET_ACCESS || 'accessKey_',
    mongodbURI: process.env.MONGODB_URI || 'mongodb://localhost:27017/prep'
  },
  development: {
    awsRegion: process.env.AWS_REGION || 'local',
    awsEndPoint: process.env.AWS_END_POINT || 'http://localhost:8000',
    mongodbURI: process.env.MONGODB_URI || 'mongodb://localhost:27017/prep'
  },
  test: {
    port: process.env.APP_PORT || 3110,
    host: process.env.APP_HOST || '127.0.0.1'
  }
}

const config = Object.assign(configs.base, configs[env])
module.exports = config
