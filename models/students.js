/**
 * models/students.js
 *
 * @description :: Describes the students functions
 * @docs        :: TODO
 */
const _ = require('underscore')

const Util = require('../util')
const { ddbClient, docClient } = require('../ddb/client')

const Groups = require('./groups')

const tableName = 'students'

const Students = {
  getById: async id => {
    const params = {
      TableName: tableName,
      Key: {
        'studentId': {
          'S': id
        }
      }
    }

    const getStudent = ddbClient.getItem(params).promise()
    const student = await getStudent.then(student => {
      return student.Item
    })

    if (student.hashed_password) {
      delete student.hashed_password
    }

    let studentData = Util.removeTypesFromObject(student)

    return {
      success: true,
      student: studentData
    }
  },

  update: async (id, data) => {
    const student = await Students.getById(id)

    if (!student.success) {
      return {
        success: false,
        code: 404,
        error: `User not found: ${id}`
      }
    }

    let params = {
      TableName: tableName,
      Key: {
        studentId: id
      },
      ReturnValues: 'UPDATED_NEW'
    }

    const updateExpression = Util.formatUpdateObject(data)

    params = _.extend(params, updateExpression)

    const updateUser = docClient.update(params).promise()
    const result = await updateUser
      .then(student => {
        return {
          success: true,
          message: `Update: ${id}`,
          student
        }
      })
      .catch(err => {
        console.error('- Error trying to update an student', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  }
}

module.exports = Students
