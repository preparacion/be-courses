/**
 * models/classRooms.js
 *
 * @description :: Describes the classRooms functions
 * @docs        :: TODO
 */
const ClassRoomsModel = require('../db/classRooms')

const ClassRooms = {
  /**
   * get
   *
   * @description: Returns all the items
   * @returns {array} - List of items
   */
  get: async () => {
    const classRooms = await ClassRoomsModel
      .find({})
      .then(classRooms => {
        return {
          success: true,
          classRooms
        }
      })
      .catch(err => {
        console.error('- Error trying to get all classRooms.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return classRooms
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id Item id
   */
  getById: async id => {
    const classRoom = await ClassRoomsModel
      .findOne({ _id: id })
      .then(classRoom => {
        if (classRoom) {
          return {
            success: true,
            classRoom
          }
        } else {
          return {
            success: false,
            code: 404,
            error: `ClassRoom not found: ${id}`
          }
        }
      })
      .catch(err => {
        console.error('- Error trying to get a classroom by id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return classRoom
  },

  create: async data => {
    const classRoom = new ClassRoomsModel(data)
    const result = await classRoom.save()
      .then(classRoom => {
        return {
          success: true,
          classRoom
        }
      })
      .catch(err => {
        console.error('- Error trying to create a classroom', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  update: async (id, data) => {
    const classroom = await ClassRooms.getById(id)

    if (!classroom.success) {
      return {
        success: false,
        code: 404,
        error: `Classroom not found: ${id}`
      }
    }

    const result = await ClassRoomsModel
      .findOneAndUpdate({
        _id: id
      }, {
        '$set': data
      })
      .then(classRoom => {
        return {
          success: true,
          classRoom
        }
      })
      .catch(err => {
        console.error('- Error trying to update a classroom', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  delete: async id => {
    const result = await ClassRoomsModel.deleteOne({ _id: id })
      .then(() => {
        return {
          success: true,
          deleted: true
        }
      })
      .catch(err => {
        console.error('- Error trying to update a classroom', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  getByOldId: async oldId => {
    const classRoom = await ClassRoomsModel
      .findOne({ oldId: oldId })
      .then(classRoom => {
        if (classRoom) {
          return {
            success: true,
            classRoom
          }
        } else {
          return {
            success: false,
            code: 404,
            error: `ClassRoom not found: ${oldId}`
          }
        }
      })
      .catch(err => {
        console.error('- Error trying to get a classroom by id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return classRoom
  }
}

module.exports = ClassRooms
