/**
 * models/schedulesCatalog.js
 *
 * @description :: Describes the schedulesCatalog functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const uuidv4 = require('uuid/v4')

const Util = require('../util')

const { ddbClient, docClient } = require('../ddb/client')

const tableName = 'schedulesCatalog'

const SchedulesCatalog = {
  /**
   * get
   *
   * @description: Returns all the items
   * @returns {array} - List of items
   */
  get: async () => {
    const getSchedules = ddbClient.scan({ TableName: tableName }).promise()
    const schedules = await getSchedules
      .then(schedules => schedules.Items)
      .catch(err => {
        console.error('- Error trying to get the class rooms.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      schedules: schedules.map(classRoom => {
        return Util.removeTypesFromObject(classRoom)
      })
    }
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id - Location id.
   */
  getById: async id => {
    const params = {
      TableName: tableName,
      Key: {
        schedulesCatalogId: {
          S: id
        }
      }
    }

    const getSchedule = ddbClient.getItem(params).promise()
    const schedule = await getSchedule.then(schedule => schedule)
      .catch(err => {
        console.error('- Error trying to get a schedule /:id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if (_.isEmpty(schedule)) {
      return {
        success: false,
        code: 404,
        error: `schedule not found: ${id}`
      }
    }

    return {
      success: true,
      schedule: Util.removeTypesFromObject(schedule.Item)
    }
  },

  /**
   * create
   *
   * @description: Returns all the items.
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const id = uuidv4()

    let params = {
      TableName: tableName,
      Item: {
        schedulesCatalogId: id
      }
    }

    params.Item = _.extend(params.Item, data)

    const createClassRoom = docClient.put(params).promise()

    const result = await createClassRoom
      .then(() => {
        return {
          success: true,
          schedulesCatalogId: id
        }
      })
      .catch(err => {
        console.error('- Error trying to create a new schedule in catalog', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  update: async (id, data) => {
    const schedule = await SchedulesCatalog.getById(id)

    if (!schedule.success) {
      return {
        code: 404,
        success: false,
        error: `Schedule not found ${id}`
      }
    }

    let params = {
      TableName: tableName,
      Key: {
        schedulesCatalogId: id
      },
      ReturnValues: 'UPDATED_NEW'
    }

    const updateExpression = Util.formatUpdateObject(data)

    params = _.extend(params, updateExpression)

    const updateSchema = docClient.update(params).promise()
    const result = await updateSchema
      .then(location => {
        return {
          success: true,
          message: `Update: ${id}`,
          location
        }
      })
      .catch(err => {
        console.error('- Error trying to update a schedule - catalog', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  /**
   * delete
   *
   * @description: Removes a single item by id.
   * @param {string} id - Location id.
   */
  delete: async id => {
    const params = {
      TableName: tableName,
      Key: {
        schedulesCatalogId: id
      }
    }

    const deleteSchedule = docClient.delete(params).promise()
    const result = await deleteSchedule
      .then(() => {
        return {
          success: true,
          message: `Deleted: ${id}`
        }
      })
      .catch(err => {
        console.error('- Error trying to delete a schedule - catalog', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  }
}

module.exports = SchedulesCatalog
