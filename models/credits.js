/**
 * models/credits.js
 *
 * @description :: Describes the credits functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const uuidv4 = require('uuid/v4')

const { ddbClient, docClient } = require('../ddb/client')

const tableName = 'credits'

const Credits = {
  getBalance: studentId => {
    // TODO
  },

  /**
   * create
   *
   * @description: Returns all the items.
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const id = uuidv4()

    let params = {
      TableName: tableName,
      Item: {
        creditId: id
      }
    }

    params.Item = _.extend(params.Item, data)

    const createLocation = docClient.put(params).promise()

    const result = await createLocation
      .then(() => {
        return {
          success: true,
          creditId: id
        }
      })
      .catch(err => {
        console.error('- Error trying to create a new credit.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  }
}

module.exports = Credits
