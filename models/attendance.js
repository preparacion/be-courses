/**
 * models/attendance.js
 *
 * @description :: Describes the attendance functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const uuidv4 = require('uuid/v4')

const { ddbClient, docClient } = require('../ddb/client')

const tableName = 'attendance'

const Attendance = {
  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async () => {
    const getAttendances = ddbClient.scan({ TableName: tableName }).promise()
    const attendances = await getAttendances
      .then(attendances => attendances.Items)
      .catch(err => {
        console.error('- Error trying to get all attendances.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      attendances
    }
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id - Course id.
   */
  getById: async id => {
    const params = {
      TableName: tableName,
      Key: {
        attendanceId: {
          S: id
        }
      }
    }

    const getAttendance = ddbClient.getItem(params).promise()
    const attendance = await getAttendance.then(attendance => attendance)
      .catch(err => {
        console.error('- Error trying to get a attendance /:id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if (_.isEmpty(attendance)) {
      return {
        success: false,
        code: 404,
        error: `Course not found: ${id}`
      }
    }

    return {
      success: true,
      attendance
    }
  },

  /**
   * create
   *
   * @description: Returns all the items.
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const id = uuidv4()

    let params = {
      TableName: tableName,
      Item: {
        attendanceId: id
      }
    }

    params.Item = _.extend(params.Item, data)

    const createAttendance = docClient.put(params).promise()

    const result = await createAttendance
      .then(() => {
        return {
          success: true,
          attendanceId: id
        }
      })
      .catch(err => {
        console.error('- Error trying to create a new attendance.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  /**
   * delete
   *
   * @description: Removes a single item by id.
   * @param {string} id - Course id.
   */
  delete: async id => {
    const params = {
      TableName: tableName,
      Key: {
        attendanceId: id
      }
    }

    const deleteAttendance = docClient.delete(params).promise()
    const result = await deleteAttendance
      .then(() => {
        return {
          success: true,
          message: `Deleted: ${id}`
        }
      })
      .catch(err => {
        console.error('- Error trying to delete a attendance', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  }
}

module.exports = Attendance
