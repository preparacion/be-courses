/**
 * models/users.js
 *
 * @description :: Describes the users functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const uuidv4 = require('uuid/v4')
const bcrypt = require('bcryptjs')

const { ddbClient, docClient } = require('../ddb/client')

const tableName = 'users'

const Users = {
  findByEmail: async (email = '') => {
    const params = {
      TableName: tableName,
      IndexName: 'EmailIndex',
      KeyConditionExpression: 'email = :email',
      ExpressionAttributeValues: {
        ':email': email
      }
    }

    const getUser = docClient.query(params).promise()
    const result = await getUser
      .then(result => result.Items)
      .catch(err => {
        console.error('- Error trying to get an user (findUserByEmail).', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return (result.length) ? result[0] : false
  },

  /**
   * getById
   *
   * @description: Returns all the items
   * @param id {string} - User id
   */
  getById: async id => {
    const params = {
      TableName: tableName,
      Key: {
        userId: {
          S: id
        }
      }
    }

    const getUser = ddbClient.getItem(params).promise()
    const user = await getUser.then(user => user)
      .catch(err => {
        console.error('- Error trying to get a user /:id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if (_.isEmpty(user)) {
      return {
        success: false,
        code: 404,
        error: `User not found: ${id}`
      }
    }

    if (user.Item.hashed_password) {
      delete user.Item.hashed_password
    }

    return {
      success: true,
      user
    }
  }
}

module.exports = Users
