/**
 * models/courses.js
 *
 * @description :: Describes the courses functions
 * @docs        :: TODO
 */
const CourseModel = require('../db/courses')

const Courses = {
  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async ({ skip, limit } = {}) => {
    const courses = await CourseModel.find()
      .then(courses => {
        return {
          success: true,
          courses
        }
      })
      .catch(err => {
        console.error('- Error trying to get all courses', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return courses
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id - Course id.
   */
  getById: async id => {
    const course = await CourseModel.findOne({ _id: id })
      .then(course => {
        return {
          success: true,
          course
        }
      })
      .catch(err => {
        console.error('- Error trying to get a course by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return course
  },

  /**
   * create
   *
   * @description: create a new item.
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const course = new CourseModel(data)
    const result = await course.save()
      .then(course => {
        return {
          success: true,
          course
        }
      })
      .catch(err => {
        console.error('- Error trying to create a course', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  /**
   * update
   *
   * @description: update an item.
   * @param {string} id - Item id
   * @param {object} data - List of attributes.
   */
  update: async (id, data) => {
    const course = await Courses.getById(id)

    if (!course.success) {
      return {
        success: false,
        code: 404,
        message: `Course not found: ${id}`
      }
    }

    const result = await CourseModel
      .findOneAndUpdate({
        _id: id
      }, {
        $set: data
      })
      .then(course => {
        return {
          success: true,
          course
        }
      })
      .catch(err => {
        console.error('- Error trying to update a course', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  delete: async id => {
    const result = await CourseModel.deleteOne({ _id: id })
      .then(() => {
        return {
          success: true,
          deleted: true
        }
      })
      .catch(err => {
        console.error('- Error trying to delete a course', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  getByType: async type => {
    const courses = await CourseModel
      .find({
        type: type
      })
      .then(courses => {
        return {
          success: true,
          courses
        }
      })
      .catch(err => {
        console.error('- Error trying to get courses by type', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return courses
  },

  getByDependency: async dependency => {
    const courses = await CourseModel
      .find({
        dependency: dependency
      })
      .then(courses => {
        return {
          success: true,
          courses
        }
      })
      .catch(err => {
        console.error('- Error trying to get courses by type', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return courses
  },

  getByOldId: async oldId => {
    const course = await CourseModel
      .findOne({
        oldId: oldId
      })
      .then(course => {
        return {
          success: true,
          course
        }
      })
      .catch(err => {
        console.error('- Error trying to get all courses', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return course
  }
}

module.exports = Courses
