/**
 * models/schedules.js
 *
 * @description :: Describes the schedules functions
 * @docs        :: TODO
 */
const ScheduleModel = require('../db/schedules')

const Schedules = {
  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async () => {
    const schedules = await ScheduleModel
      .find({})
      .then(schedules => {
        return {
          success: true,
          schedules
        }
      })
      .catch(err => {
        console.error('- Error trying to get all schedules.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return schedules
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id - Schedule id.
   */
  getById: async id => {
    const schedule = await ScheduleModel
      .findOne({ _id: id })
      .then(schedule => {
        if (schedule) {
          return {
            success: true,
            schedule
          }
        } else {
          return {
            success: false,
            code: 404,
            error: `Schedule not found: ${id}`
          }
        }
      })
      .catch(err => {
        console.error('- Error trying to get a schedule by id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return schedule
  },

  /**
   * create
   *
   * @description: Returns all the items.
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const schedule = new ScheduleModel(data)
    const result = await schedule.save()
      .then(schedule => {
        return {
          success: true,
          schedule
        }
      })
      .catch(err => {
        console.error('- Error trying to create a schedule', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  /**
   * delete
   *
   * @description: Removes a single item by id.
   * @param {string} id - Schedule id.
   */
  delete: async id => {
    const result = await ScheduleModel.deleteOne({ _id: id })
      .then(() => {
        return {
          success: true,
          deleted: true
        }
      })
      .catch(err => {
        console.error('- Error trying to update a schedule', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  getByOldId: async oldId => {
    const schedule = await ScheduleModel
      .findOne({
        oldId: oldId
      })
      .then(schedule => {
        if (schedule) {
          return {
            success: true,
            schedule
          }
        } else {
          return {
            success: false,
            code: 404,
            error: `Schedule not found: ${oldId}`
          }
        }
      })
      .catch(err => {
        console.error('- Error trying to get a schedule by id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return schedule
  }
}

module.exports = Schedules
