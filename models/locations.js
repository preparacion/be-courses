/**
 * models/locations.js
 *
 * @description :: Describes the locations functions
 * @docs        :: TODO
 */
const LocationModel = require('../db/locations')

const Locations = {
  /**
   * get
   *
   * @description: Returns all the items.
   */
  get: async () => {
    const locations = await LocationModel
      .find({})
      .then(result => result)
      .catch(err => {
        console.error('- Error trying to get all locations.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return {
      success: true,
      locations
    }
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id Item id
   */
  getById: async id => {
    const location = await LocationModel
      .findOne({ _id: id })
      .then(location => {
        if (location) {
          return {
            success: true,
            location
          }
        } else {
          return {
            success: false,
            code: 404,
            error: `Location not found: ${id}`
          }
        }
      })
      .catch(err => {
        console.error('- Error trying to get a location by id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return location
  },

  // Only used in migration process
  getByOldId: async oldId => {
    const location = await LocationModel
      .findOne({
        oldId: oldId
      })
      .then(location => {
        if (location) {
          return {
            success: true,
            location
          }
        } else {
          return {
            success: false,
            code: 404,
            error: `Location not found: ${oldId}`
          }
        }
      })
      .catch(err => {
        console.error('- Error trying to get a location by id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return location
  },

  /**
   * create
   *
   * @description: create a new item.
   * @param {object} data Collection of attributes
   */
  create: async (data) => {
    const location = new LocationModel(data)
    const result = await location
      .save()
      .then(location => {
        return location
      })
      .catch(err => {
        console.error('- Error trying to create a location', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      location: result
    }
  }

  // TODO: update method
}

module.exports = Locations
