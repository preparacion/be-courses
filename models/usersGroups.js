/**
 * models/usersGroups.js
 *
 * @description :: Describes the users functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const uuidv4 = require('uuid/v4')

const Util = require('../util')
const { ddbClient, docClient } = require('../ddb/client')

const tableName = 'usersGroups'

const UsersGroups = {
  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async ({ skip, limit } = {}) => {
    let params = {
      TableName: tableName
    }

    if (limit > 0) {
      params['Limit'] = limit
    }

    if (skip) {
      params['ExclusiveStartKey'] = {
        studentId: {
          S: skip
        }
      }
    }

    const getUsersGroups = ddbClient.scan(params).promise()
    const usersGroups = await getUsersGroups
      .then(usersGroups => {
        let result = {}
        result.usersGroups = usersGroups.Items
        if (usersGroups.LastEvaluatedKey) {
          result.skip = usersGroups.LastEvaluatedKey.usersGroupsId.S
        }
        return result
      })
      .catch(err => {
        console.error('- Error trying to get all usersGroups.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      usersGroups
    }
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id - UsersGroups id.
   */
  getById: async id => {
    const params = {
      TableName: tableName,
      Key: {
        usersGroupsId: {
          S: id
        }
      }
    }

    const getUserGroup = ddbClient.getItem(params).promise()
    const userGroup = await getUserGroup.then(userGroup => userGroup)
      .catch(err => {
        console.error('- Error trying to get a userGroup /:id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if (_.isEmpty(userGroup)) {
      return {
        success: false,
        code: 404,
        error: `Group not found: ${id}`
      }
    }

    return {
      success: true,
      userGroup
    }
  },

  /**
   * create
   *
   * @description: Returns all the items.
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const id = uuidv4()

    let params = {
      TableName: tableName,
      Item: {
        usersGroupsId: id
      }
    }

    params.Item = _.extend(params.Item, data)

    const createUsersGroup = docClient.put(params).promise()

    const result = await createUsersGroup
      .then(() => {
        return {
          success: true,
          usersGroupsId: id
        }
      })
      .catch(err => {
        console.error('- Error trying to create a new UserGroups.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  update: async (id, data) => {
    const userGroup = await UsersGroups.getById(id)

    if (!userGroup.success) {
      return {
        code: 404,
        success: false,
        error: `UsersGroup not found ${id}`
      }
    }

    let params = {
      TableName: tableName,
      Key: {
        usersGroupsId: id
      },
      ReturnValues: 'UPDATED_NEW'
    }

    const updateExpression = Util.formatUpdateObject(data)

    params = _.extend(params, updateExpression)

    const updateUserGroup = docClient.update(params).promise()
    const result = await updateUserGroup
      .then(userGroup => {
        return {
          success: true,
          message: `Update: ${id}`,
          userGroup
        }
      })
      .catch(err => {
        console.error('- Error trying to update an userGroup', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  delete: async id => {
    const params = {
      TableName: tableName,
      Key: {
        usersGroupsId: id
      }
    }

    const deleteUserGroup = docClient.delete(params).promise()
    const result = await deleteUserGroup
      .then(() => {
        return {
          success: true,
          message: `Deleted: ${id}`
        }
      })
      .catch(err => {
        console.error('- Error trying to delete a group', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  getFromUser: async userId => {
    const params = {
      TableName: tableName,
      IndexName: 'UserIndex',
      KeyConditionExpression: 'userId = :userId',
      ExpressionAttributeValues: {
        ':userId': userId
      }
    }

    const getUsersGroups = docClient.query(params).promise()
    const result = await getUsersGroups
      .then(result => result.Items)
      .catch(err => {
        console.error('- Error trying to get an user (findUserByEmail).', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return (result.length) ? { success: true, usersGroups: result }
      : { success: false, code: 404, message: `UsersGroup not found for this user: ${userId}` }
  }
}

module.exports = UsersGroups
