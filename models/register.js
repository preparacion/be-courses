/**
 * models/register.js
 *
 * @description :: Describes the register functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const uuidv4 = require('uuid/v4')
const bcrypt = require('bcryptjs')

const { ddbClient, docClient } = require('../ddb/client')

const tableName = 'users'

const Register = {
  findUserByEmail: async (email = '') => {
    const params = {
      TableName: tableName,
      IndexName: 'EmailIndex',
      KeyConditionExpression: 'email = :email',
      ExpressionAttributeValues: {
        ':email': email
      }
    }

    const getUser = docClient.query(params).promise()
    const result = await getUser
      .then(result => result.Items)
      .catch(err => {
        console.error('- Error trying to get an user (findUserByEmail).', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return (result.length) ? result : false
  },

  create: async data => {
    // Search an existing user
    if (data.email) {
      const user = await Register.findUserByEmail(data.email)

      if (!_.isEmpty(user)) {
        return {
          success: false,
          code: 400,
          error: `This email is already taken ${data.email}`
        }
      }
    }

    const id = uuidv4()

    let params = {
      TableName: tableName,
      Item: {
        userId: id
      }
    }

    if (data.password) {
      data.hashed_password = await bcrypt.hash(data.password, 10)
      delete data.password
    }

    params.Item = _.extend(params.Item, data)

    const createUser = docClient.put(params).promise()

    const result = await createUser
      .then(() => {
        return {
          success: true,
          userId: id
        }
      })
      .catch(err => {
        console.error('- Error trying to create a new user (register).', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  }
}

module.exports = Register
