/**
 * models/paymentInfo.js
 *
 * @description :: Describes the paymentInfo functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const uuidv4 = require('uuid/v4')

const { ddbClient, docClient } = require('../ddb/client')

const Utils = require('../util')

const tableName = 'paymentInfo'

const Credits = {
  /**
   * get
   *
   * @description: Returns all the items.
   */
  get: async () => {
    const getPaymentInfos = ddbClient.scan({ TableName: tableName }).promise()
    const paymentInfos = await getPaymentInfos
      .then(paymentInfos => paymentInfos.Items)
      .catch(err => {
        console.error('- Error trying to get all paymentInfos.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      paymentInfos: paymentInfos.map(paymentInfo => {
        return Utils.removeTypesFromObject(paymentInfo)
      })
    }
  },

  getById: async id => {
    const params = {
      TableName: tableName,
      Key: {
        paymentInfoId: {
          S: id
        }
      }
    }

    const getPaymentInfo = ddbClient.getItem(params).promise()
    const paymentInfo = await getPaymentInfo.then(paymentInfo => paymentInfo)
      .catch(err => {
        console.error('- Error trying to get a paymentInfo /:id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if (_.isEmpty(paymentInfo)) {
      return {
        success: false,
        code: 404,
        error: `PaymentInfo not found: ${id}`
      }
    }

    return {
      success: true,
      paymentInfo: Utils.removeTypesFromObject(paymentInfo.Item)
    }
  },

  /**
   * create
   *
   * @description: Returns all the items.
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const id = uuidv4()

    let params = {
      TableName: tableName,
      Item: {
        paymentInfoId: id
      }
    }

    params.Item = _.extend(params.Item, data)

    const createPaymentInfo = docClient.put(params).promise()

    const result = await createPaymentInfo
      .then(() => {
        return {
          success: true,
          paymentInfoId: id
        }
      })
      .catch(err => {
        console.error('- Error trying to create a new paymentInfo.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  }
}

module.exports = Credits
